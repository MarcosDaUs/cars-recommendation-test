# Recomendaciones de autos

Aplicación web utilizando React para mostrar las recomendaciones de autos según los criterios que se envíen por medio de url.

## Puesta en marcha

Pasos a seguir para ejecutar la aplicación en un entorno local usando el gestor de paquetes de Nodejs (npm):

### `npm install`

Instala todos los paquetes necesarios para la aplicación

### `npm start`

Ejecuta la aplicación en modo desarrollo. Para ver en el navegador usar:\
[http://localhost:3000](http://localhost:3000)

## Aplicación en producción

Para ver la aplicación en producción ingresé al siguiente link:\
[https://cars-recommendation-test.vercel.app/](https://cars-recommendation-test.vercel.app/)
