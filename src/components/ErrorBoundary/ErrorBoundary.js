import React from 'react';

export default class ErrorBoundary extends React.Component {
  constructor(props) {
    super(props);
    this.state = { hasError: false };
  }
  static getDerivedStateFromError(error) {
    return { hasError: true };
  }
  render() {
    if (this.state.hasError) {
      return <span>Ha ocurrido un error, por favor recarga la página presionando f5 o contacta a soporte técnico.</span>;
    }
    return this.props.children; 
  }
};