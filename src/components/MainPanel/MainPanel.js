import React, { Suspense } from 'react';
import { Route, Routes } from 'react-router-dom';

import Spinner from '../ui/Spinner/Spinner';

import styles from './MainPanel.module.css';

const HomePage = React.lazy(() => import('../../views/Home/HomePage/HomePage'));
const CarsPage = React.lazy(() => import('../../views/Cars/CarsPage/CarsPage'));

const MainPanel = () => {
  return (
    <main className={styles.MainPanel}>
      <Suspense fallback={<Spinner size={60} />}>
        <Routes>
          <Route exact path="/cars" element={<CarsPage />} />
          <Route exact path="/" element={<HomePage />} />
          <Route path="*" element={<HomePage />} />
        </Routes>
      </Suspense>
    </main>
  );
};

export default MainPanel;