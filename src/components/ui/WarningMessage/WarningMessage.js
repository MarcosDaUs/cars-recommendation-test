import React from 'react';
import PropTypes from 'prop-types';

import styles from "./WarningMessage.module.css";

const WarningMessage = React.memo((props) => {
  return (
    <div className={`${styles.WarningContainer} ${props.className}`}>
      <span>{props.message}</span>
    </div>
  );
});

WarningMessage.propTypes = {
  className: PropTypes.string,
  message: PropTypes.string
};

WarningMessage.defaultProps = {
  className: '',
  message: ''
};

export default WarningMessage;
