import React from 'react';
import PropTypes from 'prop-types';

import styles from "./Button.module.css";

const Button = React.memo((props) => {
  return (
    <button
      {...props}
      className={`${props.className} ${styles.Button}`}
    >
      {props.children}
    </button>
  );
});

Button.propTypes = {
  className: PropTypes.string
};

Button.defaultProps = {
  className: ''
};

export default Button;
