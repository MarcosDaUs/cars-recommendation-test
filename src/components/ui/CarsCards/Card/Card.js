import React from 'react';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faCar, faSuitcaseRolling, faUserFriends, faDoorOpen } from "@fortawesome/free-solid-svg-icons";

import Button from '../../Button/Button';
import WarningMessage from '../../WarningMessage/WarningMessage';

import styles from "./Card.module.css";

const Card = (props) => {
  return (
    <div className={`${styles.CardContainer} ${styles.Margin}`}>
       <div className={styles.MainImageContainer }>
        <div className={styles.CardMainImage}>
          {
            (props.car.imageUrl) ?
            <img src={props.car.imageUrl} alt='Imagen del carro' />:
            null
          }
        </div>
        <WarningMessage className={styles.WarningContainer} message={`-${props.car.promotionalDiscount ? props.car.promotionalDiscount : 0}%`} />
      </div>
      <div className={styles.MainInfoContainer}>
        <div className={styles.CardSecondImage}>
          {
            (props.car.companyImageUrl) ?
            <img src={props.car.companyImageUrl} alt='Imagen de la compañia' />:
            null
          }
        </div>
        <span className={styles.CardTitle}>{props.car?.category}</span>
        <span className={styles.CardText}>{props.car?.model}</span>
      </div>
      <div className={styles.IconsInfoContainer}>
        <span className={styles.FontIcon}><FontAwesomeIcon icon={faUserFriends} /></span>
        <span className={styles.IconText}>{props.car?.passengers}</span>
        <span className={styles.FontIcon}><FontAwesomeIcon icon={faSuitcaseRolling} /></span>
        <span className={styles.IconText}>{(props.car?.carBaggage && props.car.carBaggage.length) ? props.car.carBaggage.length : 0}</span>
        <span className={styles.FontIcon}><FontAwesomeIcon icon={faDoorOpen} /></span>
        <span className={styles.IconText}>{props.car?.doors}</span>
      </div>
      <div className={styles.PriceInfoContainer}>
        <div className={styles.CardIcon}>
          <FontAwesomeIcon icon={faCar} />
        </div>
        <span className={styles.DayPriceText}>{`USD ${props.car.totalRate ? props.car.totalRate: 0}`}<span className={styles.DayPriceSubText}>/ día</span></span>
        <span className={styles.ThreeDayPriceText}>{`USD ${props.car.totalRate ? props.car.totalRate*3: 0}/ 3 días`}</span>
      </div>
      <div className={styles.ButtonContainer}>
        <Button
          className={styles.Button}
          onClick={() => props.onShowProduct(props.car)}
        >
          <span>Reservar</span>
        </Button>
      </div>
    </div>
  );
};

export default Card;
