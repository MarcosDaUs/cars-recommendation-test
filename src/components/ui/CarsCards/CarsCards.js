import React from 'react';
import PropTypes from 'prop-types';

import Card from "./Card/Card";

import styles from "./CarsCards.module.css";

/*
Componente donde se muestran los carros como cards de información
*/

const CarsCards = (props) => {
  // Renderizar cada uno de los carros como un card
  const cardsElements = props.cars.map((car, index) => (
    <Card 
      car={car} 
      key={`${index}_${car.type}`} 
      onShowProduct={props.onShowProduct} 
    />
  ));
  return (
    <div className={`${props.className} ${styles.DivSubCardsPostsContainer}`}>
      <div className={`${styles.ColumnResponsive} ${styles.RowCardsPostsContainer} ${styles.CardsPostsContainer}`}>
        {
          cardsElements
        }
      </div>
    </div>
  );
};

CarsCards.propTypes = {
  cars: PropTypes.arrayOf(PropTypes.object), // Array de carros
  className: PropTypes.string
};

CarsCards.defaultProps = {
  className: ''
}

export default CarsCards;
