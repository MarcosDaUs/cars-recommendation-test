import React from 'react';
import PropTypes from 'prop-types';

import CircularProgress from '@mui/material/CircularProgress';

import styles from "./Spinner.module.css";

const Spinner = (props) => {
  return (
    <div className={`${styles.Spinner} ${props.className}`}>
      <CircularProgress
        size={props.size}
      />
    </div>
  );
};

Spinner.propTypes = {
  className: PropTypes.string,
  size: PropTypes.number.isRequired
};

Spinner.defaultProps = {
  className: '',
  size: 30
};

export default Spinner;
