import React from 'react';

import ErrorBoundary from '../ErrorBoundary/ErrorBoundary';
import MainPanel from '../MainPanel/MainPanel';
import MainFooter from '../MainFooter/MainFooter';

import styles from './Layout.module.css';

const Layout = () => {
  return (
    <ErrorBoundary>
      <div className={ styles.Layout }>    
        <MainPanel />
        <MainFooter />
      </div>
    </ErrorBoundary>
  );
};

export default Layout;