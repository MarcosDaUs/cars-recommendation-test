
import React from 'react';

import Logo from '../../assets/img/destino-jet-travel.png';

import styles from "./MainFooter.module.css";

const MainFooter = () => {
  return (
    <footer className={`${styles.Footer}`}>
      <span className={styles.Text}>Powered by</span>
      <div className={styles.Logo}>
        <img src={Logo} alt="Logo" />
      </div>
    </footer>
  );
}

export default MainFooter;
