import { configureStore } from '@reduxjs/toolkit'
import authReducer from './auth';

export default configureStore({
  reducer: {
    auth: authReducer,
  },
  devTools: process.env.NODE_ENV !== 'production',
});

/*
const store = createStore(reducer, composeEnhancers(
    applyMiddleware(thunk)
));
*/