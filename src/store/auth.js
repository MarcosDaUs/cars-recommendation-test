import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';

import apiInstance from '../services/axios/apiInstance';

import { setToken, clearToken } from '../services/localStorage/localStorageService';

import { AGENCY_ID, AGENCY_KEY, CLIENT_ID } from '../utils/global';
import { GET_AGENCY_TOKEN } from '../utils/endPoints';

const initialState = {
    isLoading: false,
    token: null,
    error: null,
};

export const authCheckState = createAsyncThunk(
  'authentication/authCheckState',
  ( payload ) => new Promise( (resolve, reject) => {
    clearToken();
    apiInstance({
        url: GET_AGENCY_TOKEN,
        method: 'post',
        data: {
          agencyId: AGENCY_ID,
          agencyKey: AGENCY_KEY,
          clientId: CLIENT_ID
        },
    })
    .then(function (response) {
      if ((response.status === 200) && response.data) {
        setToken(response.data);
        resolve(response.data);
      } else {
        reject('Error desconocido, por favor recarga la página presionando f5 o contacta a soporte técnico.');
      }
    })
    .catch((error) => {
      if (error.response.status === 401) {
        reject('Error con la autenticación, por favor recarga la página presionando f5 o contacta a soporte técnico.');
      } else {
        reject('Error desconocido, por favor recarga la página presionando f5 o contacta a soporte técnico.');
      }
        
    });     
  })
);

const authSlice = createSlice({
  name: 'authentication',
  initialState: initialState,
  reducers: {
    setAuthToken: (state, action) => {
      setToken(action.payload);
      state.isLoading = false;
      state.token = action.payload;
      state.error = null;
    }
  },
  extraReducers: {
    [ authCheckState.pending ]: (state) => {
      state.isLoading = true;
    },
    [ authCheckState.fulfilled ]: (state, action) => {
      state.isLoading = false;
      state.token = action.payload;
      state.error = null;
    },
    [ authCheckState.rejected ]: (state, action) => {
      clearToken();
      state.isLoading = false;
      state.token = null;
      state.error = action.error.message;
    }
  }
});

export const { setAuthToken } = authSlice.actions;

export default authSlice.reducer;