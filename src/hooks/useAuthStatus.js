import { useEffect, useState } from 'react';
import { useSelector, useDispatch } from "react-redux";

import { authCheckState, setAuthToken } from '../store/auth';

import { getToken } from '../services/localStorage/localStorageService';

const selector = (state) => {
  return {
      isAuthenticated: (state.auth.token !== null),
      isLoading: state.auth.isLoading,
      token: state.auth.token,
      error: state.auth.error,
  };
};

export default function useAuthStatus () {
  const dispatch = useDispatch();
  const [showState, setShowState] = useState(false);
  const { isAuthenticated, isLoading, token, error } = useSelector(selector);
  useEffect(() => {
    const tempToken = getToken();
    if (tempToken) {
      dispatch(setAuthToken(tempToken));
    } else {
      dispatch(authCheckState());
    }
  }, [dispatch]);

  useEffect(() => {
    setShowState((!isLoading) && (isAuthenticated) && (error === null));
  }, [isLoading, isAuthenticated, error]);

  return {
    isAuthenticated: isAuthenticated,
    showPage: showState,
    token: token,
    isLoading: isLoading,
    error: error
  };
}