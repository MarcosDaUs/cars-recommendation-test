import { useCallback } from 'react';

import { useDispatch } from "react-redux";

import produce from "immer";

import { useImmerReducer } from "use-immer";

import { authCheckState } from '../store/auth';

import apiInstance from '../services/axios/apiInstance';

import { REQUEST_START, REQUEST_SUCCESS, REQUEST_FAIL } from '../utils/global';

const useApiRequest = ( token ) => {
  const dispatch = useDispatch();
  const [requestState, requestDispatch] = useImmerReducer(
    (draft, action) => {
      switch (action.type) {
        case REQUEST_START:
          draft.loading = true;
          draft.data = null;
          draft.error = null; 
          break;
        case REQUEST_SUCCESS:
          draft.loading = false;
          draft.data = (action.payload && action.payload.data) ? action.payload.data : 'ok';
          break;
        case REQUEST_FAIL:
          draft.loading = false;
          draft.error = (action.payload && action.payload.error) ? action.payload.error : 'unknown'; 
          break;
        default:
          draft.loading = false;
          draft.data = null;
          draft.error = null;
      }
    },
    {
      loading: false,
      data: null,
      error: null
    }
  );
  const startRequest = useCallback(( config ) => {
    requestDispatch({ type: REQUEST_START });
    apiInstance(produce(config, draft => {
      if (draft.headers) {
        draft.headers.Authorization = 'Bearer '+token;
      } else {
        draft.headers = { 'Authorization': 'Bearer '+token };
      } 
    }))
    .then( (response) => {
      if (response.data) {
        requestDispatch({ 
          type: REQUEST_SUCCESS, 
          payload: {
            data: response.data
          } 
        });     
      } else if (response.statusText && (response.statusText.toUpperCase() === 'NO CONTENT'.toUpperCase())) {
        requestDispatch({ 
          type: REQUEST_SUCCESS, 
          payload: {
            data: null
          } 
        });
      } else {
        requestDispatch({ 
          type: REQUEST_FAIL, 
          payload: {
            error: 'Respuesta corrupta.'
          } 
        });
      }
    })
    .catch( (error) => {
      let errorMessage = 'Error desconocido.';
      if (error.response.status === 401) {
        errorMessage = 'No autorizado';
        dispatch(authCheckState());
      } 
      requestDispatch({ 
        type: REQUEST_FAIL, 
        payload: {
          error: { 
            status: error.response.status, 
            data: errorMessage
          }
        } 
      });
    });
  }, [ token, dispatch, requestDispatch ]);
  return [ requestState, startRequest ];
};

export default useApiRequest;