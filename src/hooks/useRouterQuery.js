import { useMemo } from 'react';

import { useLocation } from "react-router-dom";

const handleGetRouterQuery = (search, params) => {
  let query = new URLSearchParams(search);
  let newQuery = {};
  for (let param of params) {
    if (query.has(param)) {
      newQuery[param] = (query.getAll(param).length > 1) ? query.getAll(param) : query.get(param);
    }
  }
  return {...newQuery};
};

const useRouterQuery = (params) => {
  const { search } = useLocation();
  const query = useMemo(() => handleGetRouterQuery(search, params), [search, params]);
  return {
    query: query,
    allParams: Object.keys(query).length === params.length
  };
}

export default useRouterQuery;