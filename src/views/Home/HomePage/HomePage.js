import React from 'react';

import {
  Link
} from "react-router-dom";

import styles from './HomePage.module.css';

const params = {
  pickUpCode: encodeURIComponent('MIA'), 
  pickUpCountry: encodeURIComponent('US'), 
  pickUpRegion: encodeURIComponent('unitedStates'),  
  dropOffCode: encodeURIComponent('MIA'), 
  dropOffCountry: encodeURIComponent('US'),  
  dropOffRegion: encodeURIComponent('unitedStates'), 
  pickUpDate: encodeURIComponent("2022-02-08"), 
  pickUpTime: encodeURIComponent("0930"), 
  dropOffDate: encodeURIComponent("2022-02-10"), 
  dropOffTime: encodeURIComponent("0930"), 
  rateTypeCode: encodeURIComponent(0),  
};

const HomePage = () => {
  return (
    <div className={styles.HomePage}>
      <span className={styles.Text}>Link de ejemplo para realizar una recomendación de carros:</span>
      <Link to={`/cars?pickUpCode=${params.pickUpCode}&pickUpCountry=${params.pickUpCountry}&pickUpRegion=${params.pickUpRegion}&dropOffCode=${params.dropOffCode}&dropOffCountry=${params.dropOffCountry}&dropOffRegion=${params.dropOffRegion}&pickUpDate=${params.pickUpDate}&pickUpTime=${params.pickUpTime}&dropOffDate=${params.dropOffDate}&dropOffTime=${params.dropOffTime}&rateTypeCode=${params.rateTypeCode}`}>
      {`/cars?pickUpCode=${params.pickUpCode}&pickUpCountry=${params.pickUpCountry}&pickUpRegion=${params.pickUpRegion}&dropOffCode=${params.dropOffCode}&dropOffCountry=${params.dropOffCountry}&dropOffRegion=${params.dropOffRegion}&pickUpDate=${params.pickUpDate}&pickUpTime=${params.pickUpTime}&dropOffDate=${params.dropOffDate}&dropOffTime=${params.dropOffTime}&rateTypeCode=${params.rateTypeCode}`}
      </Link>
      <span className={styles.SecondText}>Parámetros necesarios en la URL:</span>
      <ul>
        <li>pickUpCode: <span className={styles.DescriptionText}>Código de la ciudad de origen</span></li>
        <li>pickUpCountry: <span className={styles.DescriptionText}>Código del país de origen</span></li>
        <li>pickUpRegion: <span className={styles.DescriptionText}>Nombre de la región de origen</span></li>
        <li>dropOffCode: <span className={styles.DescriptionText}>Código de la ciudad de destino</span></li>
        <li>dropOffCountry: <span className={styles.DescriptionText}>Código del país de destino</span></li>
        <li>dropOffRegion: <span className={styles.DescriptionText}>Nombre de la región de destino</span></li>
        <li>pickUpDate: <span className={styles.DescriptionText}>Fecha de cuando se va a tomar el servicio (formato YYYY-MM-dd)</span></li>
        <li>pickUpTime: <span className={styles.DescriptionText}>Hora de cuando se va a tomar el servicio (formato HH:mm)</span></li>
        <li>dropOffDate: <span className={styles.DescriptionText}>Fecha de cuando se va a entregar el servicio (formato YYYY-MM-dd)</span></li>
        <li>dropOffTime: <span className={styles.DescriptionText}>Hora de cuando se va a entregar el servicio (formato HH:mm)</span></li>
        <li>rateTypeCode: <span className={styles.DescriptionText}>Código del tipo de tarifa</span></li>
      </ul>
    </div>
  );
};

export default HomePage;