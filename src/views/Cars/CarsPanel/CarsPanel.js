import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';

import useApiRequest from '../../../hooks/useApiRequest';

import { CAR_SEARCH } from '../../../utils/endPoints';
import { CLIENT_ID } from '../../../utils/global';

import CarsCards from '../../../components/ui/CarsCards/CarsCards';
import Spinner from '../../../components/ui/Spinner/Spinner';
import WarningMessage from '../../../components/ui/WarningMessage/WarningMessage';

import styles from './CarsPanel.module.css';

/*
Componente donde se muestra la información del usuario y los carros recomendados
*/

const CarsPanel = React.memo((props) => {
  // Obtener la herramienta para hacer la búsqueda en la API
  const [apiUser, startUserRequest] = useApiRequest(props.token);
  const [carsState, setCarsState] = useState(null);
  useEffect(() => {
    // Cada vez que se cambia alguno de los parámetros o el token, se debe hacer la petición y obtener los carros a mostrar en el panel
    startUserRequest({
      method: 'post',
      url: CAR_SEARCH,
      data: {
        PickUpLocation: {
          cityCode: props.query.pickUpCode,
          countryCode: props.query.pickUpCountry,
          region: props.query.pickUpRegion,
        },
        DropOffLocation: {
          cityCode: props.query.dropOffCode,
          countryCode: props.query.dropOffCountry,
          region: props.query.dropOffRegion,
        },
        PickUpDate: props.query.pickUpDate,
        PickUpTime: props.query.pickUpTime,
        DropOffDate: props.query.dropOffDate,
        DropOffTime: props.query.dropOffTime,
        RateTypeCode: props.query.rateTypeCode,
        CarCategoryType: 0,
        PaymentType: 0,
        corporateDiscount: "0",
        AgencyUrl: CLIENT_ID
      }
    });
  }, [
    startUserRequest, 
    props.query.pickUpCode, 
    props.query.pickUpCountry, 
    props.query.pickUpRegion, 
    props.query.dropOffCode, 
    props.query.dropOffCountry, 
    props.query.dropOffRegion, 
    props.query.pickUpDate, 
    props.query.pickUpTime, 
    props.query.dropOffDate, 
    props.query.dropOffTime, 
    props.query.rateTypeCode
  ]);
  useEffect(() => {
    // Verificar la información obtenida de la petición
    if (apiUser.data) {
      setCarsState(apiUser.data);
    } else {
      setCarsState(null);
    }
  }, [
    apiUser.data, 
    apiUser.error
  ]);
  let content = <Spinner size={60} />;
  if ((!apiUser.loading) && (carsState !== null) && Array.isArray(carsState) && (carsState.length > 0)) { // Verificar si termino la petición y se obtuvieron los carros 
    content = <CarsCards 
      cars={carsState}  
      onShowProduct={(data) => console.log(data)} 
    />;
  } else if ((!apiUser.loading) && (carsState !== null)) { // Verificar si termino la petición, pero no se obtuvo ningun carro 
    content =  <WarningMessage message="No se encontraron carros para recomendar" />;
  } else if ((!apiUser.loading) && (apiUser.error !== null)) { // Verificar si termino la petición y se obtuvo un error
    content = <WarningMessage message={apiUser.error ? apiUser.error : 'Error desconocido, por favor recarga la página presionando f5 o contacta a soporte técnico'} />;
  }
  return (
    <div className={styles.CarsPanel}>
      <span className={styles.MainText}>Hola Pedro,</span>
      <span className={styles.SecondText}>Ahorra hasta un 30% agregando un auto en tu viaje a Chicago.</span>
      {
        content
      }
    </div>
  );
});

CarsPanel.propTypes = {
  // Parámetros para realizar la búsqueda en la API
  query: PropTypes.shape({
    pickUpCode: PropTypes.string.isRequired, // Código de la ciudad de origen
    pickUpCountry: PropTypes.string.isRequired, // Código del país de origen
    pickUpRegion: PropTypes.string.isRequired, // Nombre de la región de origen
    dropOffCode: PropTypes.string.isRequired, // Código de la ciudad de destino
    dropOffCountry: PropTypes.string.isRequired, // Código del país de destino
    dropOffRegion: PropTypes.string.isRequired, // Nombre de la región de destino
    pickUpDate: PropTypes.string.isRequired, // Fecha de cuando se va a tomar el servicio (formato YYYY-MM-dd)
    pickUpTime: PropTypes.string.isRequired, // Hora de cuando se va a tomar el servicio (formato HHmm)
    dropOffDate: PropTypes.string.isRequired, // Fecha de cuando se va a entregar el servicio (formato YYYY-MM-dd)
    dropOffTime: PropTypes.string.isRequired, // Hora de cuando se va a entregar el servicio (formato HHmm)
    rateTypeCode: PropTypes.string.isRequired, // Código del tipo de tarifa
  }).isRequired,
  token: PropTypes.string.isRequired // Toquen de autenticación de la API
};


export default CarsPanel;