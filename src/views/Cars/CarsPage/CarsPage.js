import React from 'react';

import useAuthStatus from '../../../hooks/useAuthStatus';
import useRouterQuery from '../../../hooks/useRouterQuery';

import Spinner from '../../../components/ui/Spinner/Spinner';
import WarningMessage from '../../../components/ui/WarningMessage/WarningMessage';

import CarsPanel from '../CarsPanel/CarsPanel';

const params = [
  'pickUpCode', 
  'pickUpCountry', 
  'pickUpRegion', 
  'dropOffCode', 
  'dropOffCountry', 
  'dropOffRegion',
  'pickUpDate',
  'pickUpTime',
  'dropOffDate',
  'dropOffTime',
  'rateTypeCode'
];

/*
Componente principal de la página "/cars"
*/

const CarsPage = () => {
  // Obtener los parámetros de la URL
  const { query, allParams } = useRouterQuery(params);
  // Obtener la autenticación con la API
  const { showPage, token, error } = useAuthStatus();
  let content = <Spinner size={60} />;
  if (!allParams) { // Verificar que la URL tiene todos los parámetros necesarios para realizar la búsqueda en la API
    content = <WarningMessage message="URL incorrecta" />;
  } else if (showPage && query) { // Verificar que ya se autenticó con la API y se tienen los parámetros 
    content = <CarsPanel token={token} query={query}/>;
  } else if (error) { // Verificar si hubo un error con la autenticación de la API
    content = <WarningMessage message={error} />;
  }
  return content;
};

export default CarsPage;